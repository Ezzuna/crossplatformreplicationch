#ifndef ROCK_H_
#define ROCK_H_

#include "GameObject.h"
#include "World.h"
class Rock : public GameObject
{
public:
	CLASS_IDENTIFICATION('ROCK', GameObject)

		enum ERockReplicationState
	{
		EMRS_Pose = 1 << 0,
		EMRS_Color = 1 << 1,

		EMRS_AllState = EMRS_Pose | EMRS_Color
	};

	static	GameObject* StaticCreate() { return new Rock(); }

	virtual uint32_t	GetAllStateMask()	const override { return EMRS_AllState; }

	virtual uint32_t	Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;
	virtual void		Read(InputMemoryBitStream& inInputStream) override;

	virtual bool HandleCollisionWithPlayer(Player* Player) override;

protected:
	Rock();

};
#endif // ROCK_H_