#include "SpriteComponent.h"
#include "Bullet.h"
class BulletClient : public Bullet
{
public:
	static	GameObjectPtr	StaticCreate() { return GameObjectPtr(new BulletClient()); }

	virtual void		Read(InputMemoryBitStream& inInputStream) override;
	virtual bool		HandleCollisionWithPlayer(Player* inCat) override;

protected:
	BulletClient();

private:

	SpriteComponentPtr	mSpriteComponent;
};

typedef shared_ptr< BulletClient >	BulletClientPtr;